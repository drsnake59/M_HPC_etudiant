#include <fstream>
#include <iostream>
#include <thread>
#include <vector>

namespace Fibo {

  // calcule le Nieme terme de la suite de "Fibonacci modulo 42"
  // precondition : N >= 0
  int FibonacciMod42(int N) {
    int f_curr = 0;
    int f_prec = 1;
    for (int i=1; i<=N; i++) {
      int tmp = f_curr;
      f_curr = (f_curr + f_prec) % 42;
      f_prec = tmp;
    }
    return f_curr;
  }

  //////////////////////////////////////////////////////////////////////

  // fonction pour repartir les calculs
 void calculerTout(std::vector<int> &data) {
    // effectue tous les calculs
    for (unsigned i=0; i<data.size(); i++) {
      data[i] = FibonacciMod42(i);
    }
  };

    // fonction pour repartir les calculs
  void calculerMoitie(std::vector<int> &data,int debut,int fin) {
    // effectue tous les calculs
    for (int i=debut; i<=fin; i++) {
      data[i] = FibonacciMod42(i);
    }
  };

      // fonction pour repartir les calculs
  void calculerBloc(std::vector<int> &data,int debut,int inc) {
    // effectue tous les calculs
    for (int i=debut; i<=fin; i+=inc) {
      data[i] = FibonacciMod42(i);
    }
  };
  
  std::vector<int> fiboSequentiel(int nbData) {//long
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calcule les donnees sequentiellement
    calculerTout(data);
    return data;
  }
  
  //////////////////////////////////////////////////////////////////////

  std::vector<int> fiboBlocs(int nbData) {//court
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData);
    
    std::thread thread1(calculerMoitie,std::ref(data),0,data.size()/2);  // appel non-bloquant
    std::thread thread2(calculerMoitie,std::ref(data),data.size()/2+1,data.size());  // appel non-bloquant
    
    // attend la fin des calculs
    thread1.join();  // appel bloquant
    thread2.join();  // appel bloquant
    // calculer sur deux threads, par bloc
    // TODO
    return data;
  }

  //////////////////////////////////////////////////////////////////////

  std::vector<int> fiboCyclique2(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur deux threads, cycliquement
     std::thread thread1(calculerBloc,std::ref(data),0,2);
     std::thread thread1(calculerBloc,std::ref(data),1,2);
     
    // TODO
    return data;
  }


  //////////////////////////////////////////////////////////////////////

  std::vector<int> fiboCycliqueN(int nbData, int nbProc) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur N threads, cycliquement
    // TODO
    return data;
  }

  //////////////////////////////////////////////////////////////////////

  void fiboCycliqueNFake(int nbData, int nbProc) {
    // calculer sur N threads, cycliquement, en ignorant le résultat
    // TODO
  }

}  // namespace Fibo

